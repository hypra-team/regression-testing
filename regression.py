# Regression testing facilities

# Copyright (c) 2019-2020 Samuel Thibault <sthibault@hypra.fr>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from dogtail.config import config
from dogtail import predicate
import dogtail.rawinput
import os
import sys
import time
import subprocess
import socket
import pyatspi
import threading
import re
import traceback

config.typingDelay = 0
config.defaultDelay = 0
os.environ["LC_ALL"] = "C"

latency = 0.01
timeout = 100

red = "\033[31m"
green = "\033[32m"
normal = "\033[0m"

npassed = 0
nfailed = 0

p = None
p2 = None
f = None

toBeReachable = [
    pyatspi.ROLE_TREE_ITEM,
    pyatspi.ROLE_PAGE_TAB,
    pyatspi.ROLE_PUSH_BUTTON,
    pyatspi.ROLE_CHECK_BOX,
    pyatspi.ROLE_SPIN_BUTTON,
    pyatspi.ROLE_ENTRY,
    pyatspi.ROLE_TABLE_ROW,
    pyatspi.ROLE_DOCUMENT_WEB,
]

switch_focus_timeout = 1

lastZoom = None
waitingZoom = None

def handleZoomEvent(event):
    if not hasattr(event, "sender"):
        return
    if event.sender.name != "compiz":
        return
    lastZoom = event.source
    if waitingZoom != None and lastZoom == waitingZoom:
        pyatspi.Registry.stop()

def init():
    pyatspi.Registry.registerEventListener(handleZoomEvent, "screen-reader:region-changed")

def title(msg):
    """Print title in test log, for instance:
    >>> title('foobar tests')"""

    print("\n****** %s ******" % msg)

def passed(msg):
    """Print a 'passed' message and count it. For instance:
    >>> passed('waiting for foo')"""

    global npassed

    npassed += 1
    print("[%sPASS%s] %s" % (green, normal, msg))

def failed(msg):
    """Print a 'failed' message and count it. For instance:
    >>> failed('waiting for foo')"""

    global nfailed

    nfailed += 1
    print("[%sFAIL%s] %s" % (red, normal, msg))
    stack = traceback.extract_stack()
    for i in reversed(traceback.format_list(stack)):
        if not "regression.py" in i:
            print(i)
            break
    if os.environ.get('REGDEBUG', False):
        breakpoint()

def info(msg):
    """Print an informational message. For instance:
    >>> info('hello')"""

    print("[%sINFO%s] %s" % ('', '', msg))

def child(parent, role, identifier, **kwargs):
    """Return a child of some parent that has a given role and a given
    accessible id. For instance:
    >>> acc = child(win, 'menu', 'my_menu')"""
    return parent.child(roleName=role, identifier=identifier, **kwargs)

def childPerId(parent, identifier, **kwargs):
    """Return a child of some parent that has a given accessible id. For
    instance:
    >>> acc = childPerIdOnly(win, 'my_menu')"""
    return parent.child(identifier=identifier, **kwargs)

def childPerTag(parent, tag, **kwargs):
    """Returns a child of some parent that has a given tag in the accessible
    space-separated list attribute "tag". For instance:
    >>> acc = childPerTag(win, 'tree')"""
    def pred(child):
        return ('tag' in child.get_attributes() and
                tag in child.get_attributes()['tag'].split(' '))

    return parent.findChild(pred, debugName='child with a "tag" attribute containing "%s"' % tag,
                            **kwargs)

def childPerName(parent, role, name, **kwargs):
    """Return a child of some parent that has a given role and a given name. For
    instance:
    >>> acc = childPerName(win, 'menu', 'My menu')"""
    return parent.child(roleName=role, name=name, **kwargs)

def childPerRole(parent, role, **kwargs):
    """Return a child of some parent that has a given role. For
    instance:
    >>> acc = childPerRole(win, 'menu')"""
    return parent.child(roleName=role, **kwargs)

def nchildPerRole(parent, role, i, **kwargs):
    """Return the i-th child of some parent that has a given role. For
    instance:
    >>> acc = nchildPerRole(win, 'menu', 0)"""
    return parent.findChildren(predicate.GenericPredicate(roleName=role, **kwargs))[i]

def childPerTable(parent, i, j, **kwargs):
    """Return the (i,j) table cell of some parent table. For
    instance:
    >>> acc = childPerTable(parent, 0, 2)"""
    return parent.queryTable().getAccessibleAt(i, j)

def press(obj, k):
    """Simulate a given keypress.
    It is a bit more efficient than calling pressCombo.
    For instance:
    >>> press(acc, 'a')"""

    info("pressing '%s'" % k)
    dogtail.rawinput.pressKey(k)

def pressCombo(obj, k):
    """Simulate a given key combo.
    The available modifiers are control, alt, shift.
    For instance:
    >>> pressCombo(acc, '<control><alt>a')"""

    info("pressing '%s'" % k)
    dogtail.rawinput.keyCombo(k)

def typeText(obj, s):
    """Type some text
    For instance:
    >>> typeText(acc, 'hello')"""

    info("typing '%s'" % s)
    dogtail.rawinput.typeText(s)

def _getName(o):
    attrs = o.get_attributes()
    if 'id' in attrs:
        return attrs['id']
    else:
        return o.name

def __waitFoo(o, f, msg):
    for i in range(timeout):
        if f(o):
            return True
            return
        time.sleep(latency)
    return False

def _waitFoo(o, f, msg):
    name = _getName(o)
    res = __waitFoo(o, f, msg)
    if (res):
        passed("%s '%s' got %s" % (o.getRoleName(), name, msg))
    else:
        failed("%s '%s' didn't get %s" % (o.getRoleName(), name, msg))

def findFocused(o):
    def dive(o):
        if o.focused:
            info("%s '%s' has focus" % (o.getRoleName(), o.name))
            return o
        else:
            for oo in o:
                r = dive(oo)
                if r is not None:
                    return r
            return None

    while o.parent != None:
        o = o.parent
    return dive(o)

def waitFocus(o):
    """Wait for an object to become focused. For instance:
    >>> acc = win.child(roleName='menu', identifier='my_menu')
    >>> waitFocus(acc)"""

    _waitFoo(o, lambda o: o.focused, "focus")
    if o.focused:
        return True

    # Didn't get focused, see who got focused instead
    findFocused(o)
    return False

def waitNotFocus(o):
    """Wait for an object to become not focused. For instance:
    >>> acc = win.child(roleName='menu', identifier='my_menu')
    >>> waitNotFocus(acc)"""

    _waitFoo(o, lambda o: not o.focused, "unfocused")

def _waitNotFocus(o):
    return __waitFoo(o, lambda o: not o.focused, "unfocused")

def waitChecked(o):
    """Wait for an object to become checked. For instance:
    >>> acc = win.child(roleName='check box', identifier='my_box')
    >>> waitChecked(acc)"""

    _waitFoo(o, lambda o: o.checked, "checked")

def waitNotChecked(o):
    """Wait for an object to become unchecked. For instance:
    >>> acc = win.child(roleName='check box', identifier='my_box')
    >>> waitNotChecked(acc)"""

    _waitFoo(o, lambda o: not o.checked, "unchecked")

def waitSelected(o):
    """Wait for an object to become selected. For instance:
    >>> acc = win.child(roleName='page tab', identifier='my_tab')
    >>> waitSelected(acc)"""

    _waitFoo(o, lambda o: o.selected, "selected")

def waitNotSelected(o):
    """Wait for an object to become unselected. For instance:
    >>> acc = win.child(roleName='page tab', identifier='my_tab')
    >>> waitNotSelected(acc)"""

    _waitFoo(o, lambda o: not o.selected, "unselected")

def waitExpanded(o):
    """Wait for an object to become expanded. For instance:
    >>> acc = win.child(roleName='menu', identifier='my_menu')
    >>> waitExpanded(acc)"""

    _waitFoo(o, lambda o: o.getState().contains(pyatspi.STATE_EXPANDED) or o.getState().contains(pyatspi.STATE_SELECTED), "expanded")

def waitNotExpanded(o):
    """Wait for an object to become unexpanded. For instance:
    >>> acc = win.child(roleName='menu', identifier='my_menu')
    >>> waitNotExpanded(acc)"""

    _waitFoo(o, lambda o: not o.getState().contains(pyatspi.STATE_EXPANDED) and not o.getState().contains(pyatspi.STATE_SELECTED), "unexpanded")

def waitCaret(o, i):
    """Wait for an object to get caret position at i. For instance:
    >>> acc = win.child(roleName='entry', identifier='myentry')
    >>> waitCaret(acc, 2)"""

    _waitFoo(o, lambda o: o.queryText().get_caretOffset() == i, "caret offset %u" % i)
    offset = o.queryText().get_caretOffset()
    if offset == i:
        return True

    # Didn't get proper caret position, see the position
    info("%s '%s' has caret offset %u" % (o.getRoleName(), o.name, offset))
    return False

def waitSelection(o, i, j, n=0):
    """Wait for an object to get selection n at (i,j). For instance:
    >>> acc = win.child(roleName='entry', identifier='myentry')
    >>> waitSelection(acc, 2, 3)"""

    _waitFoo(o, lambda o: o.queryText().getSelection(n) == (i,j), "selection %u, %u" % (i, j))
    sel = o.queryText().getSelection(n)
    if sel == (i,j):
        return True

    # Didn't get proper caret position, see the position
    info("%s '%s' has selection %u, %u" % (o.getRoleName(), o.name, sel[0], sel[1]))
    return False

def selectItem(itemname, o, name=None, identifier=None, key="Down", fix=None):
    if name:
        info("Trying to select item '%s' in '%s' '%s' with key '%s'" % (name, o.getRoleName(), o.name, key))
    else:
        info("Trying to select item id '%s' in '%s' '%s' with key '%s'" % (identifier, o.getRoleName(), o.name, key))
    for i in range(100):
        # Look for the currently selected item
        for i in range(timeout):
            current = None
            for item in o:
                if item.focused:
                    current = item
                    break
            if current != None:
                break
            # None yet, wait a bit
            time.sleep(latency)

        if current == None:
            info("no item focused in '%s' '%s', try to focused some" % (o.getRoleName(), o.name))
            press(o, key)
            continue

        if name:
            if _nameMatch(name, current.name):
                passed("%s '%s' '%s' got selected" % (itemname, current.getRoleName(), current.name))
                return
        if identifier:
            attrs = current.get_attributes()
            if 'id' in attrs and identifier == attrs['id']:
                passed("%s '%s' '%s' got selected" % (itemname, current.getRoleName(), identifier))
                return

        info("item '%s' focused, pressing '%s'" % (current.name, key))
        # Too bad, try next item
        pressCombo(o, key)
        res = _waitNotFocus(current)
        if not res:
            if fix:
                info("item '%s' seems to be stuck, trying to press '%s'" % (current.name, fix))
                pressCombo(o, fix)
            else:
                info("item '%s' seems to be stuck :/" % current.name)


def _selectMenuItem(m, name=None, identifier=None):
    """Select a given item in a menu. For instance:
    >>> acc = win.child('menu', 'my_menu')
    >>> _selectMenuItem(acc, identifier='the_menu_item')"""
    selectItem("menu item", m, name=name, identifier=identifier)

def selectMenuItem(m, identifier):
    """Select a given item in a menu. For instance:
    >>> acc = win.child('menu', 'my_menu')
    >>> selectMenuItem(acc, 'the_menu_item')"""
    _selectMenuItem(m, identifier=identifier)

def selectMenuItemPerName(m, name):
    """Select a given item in a menu. For instance:
    >>> acc = win.child('menu', 'my_menu')
    >>> selectMenuItem(acc, 'The menu item')"""
    _selectMenuItem(m, name=name)

def _selectPageTab(l, name=None, identifier=None):
    """Select a given page tab in a page tab list. For instance:
    >>> acc = win.child('page tab list', 'my_list')
    >>> _selectPageTab(acc, identifier='the_page_tab')"""
    selectItem("dialog page tab", l, name=name, identifier=identifier, key="Right", fix="<shift>Tab")

def selectPageTab(l, identifier):
    """Select a given page tab in a page tab list. For instance:
    >>> acc = win.child('page tab list', 'my_list')
    >>> selectPageTab(acc, 'the_page_tab')"""
    _selectPageTab(l, identifier=identifier)

def selectPageTabPerName(l, name):
    """Select a given page tab in a page tab list. For instance:
    >>> acc = win.child('page tab list', 'my_list')
    >>> selectPageTab(acc, 'The page tab')"""
    _selectPageTab(l, name=name)

def _selectTreeItem(t, name=None, identifier=None):
    """Select a given item in a tree. For instance:
    >>> acc = win.child('tree', 'my_tree')
    >>> _selectTreeItem(acc, identifier='the_tree_item')"""
    press(t, 'Home')
    selectItem("tree item", t, name=name, identifier=identifier)

def selectTreeItem(t, identifier):
    """Select a given item in a tree. For instance:
    >>> acc = win.child('tree', 'my_tree')
    >>> selectTreeItem(acc, 'the_tree_item')"""
    press(t, 'Home')
    _selectTreeItem(t, identifier=identifier)

def selectTreeItemPerName(t, name):
    """Select a given item in a tree. For instance:
    >>> acc = win.child('tree', 'my_tree')
    >>> selectTreeItem(acc, 'The tree item')"""
    press(t, 'Home')
    _selectTreeItem(t, name=name)

def _selectButton(w, name=None, identifier=None):
    """Select a given button in a dialog. For instance:
    >>> _selectButton(win, identifier='the_button')"""
    selectItem("button", w, name=name, identifier=identifier, key="Tab")

def selectButton(w, identifier):
    """Select a given button in a dialog. For instance:
    >>> selectButton(win, 'the_button')"""
    _selectButton(w, identifier=identifier)

def selectButtonPerName(w, name):
    """Select a given button in a dialog. For instance:
    >>> selectButton(win, 'The button')"""
    _selectButton(w, name=name)

def _nameMatch(pattern, name):
    """try to match a name against a pattern"""
    if name == pattern:
        return True
    pattern = pattern + '$'
    if pattern[0] == '*':
        pattern = "\\" + pattern
    # Escape all parentheses, since grouping will never be needed here
    pattern = re.sub('([\(\)])', r'\\\1', pattern)
    match = re.match(pattern, name)
    return match is not None


def _getWindow(app, name=None, identifier=None,
               timeout=timeout, latency=latency):
    """Get a window with a given ID or name to show up. For instance:
    >>> w = _getWindow(app, identifier='the_window')

    The window is identified with the `name` through `_nameMatch()` if
    provided, and if that fails and `identifier` is provided, with the `id`
    attribute.

    `timeout` attempts are made, waiting for `latency` seconds (possibly
    fractional) between two attempts.  By default, `timeout` and `latency` use
    the global values.
    """
    for i in range(timeout):
        for f in app:
            try:
                if f and f.getState().contains(pyatspi.STATE_ACTIVE):
                    if name and _nameMatch(name, f.name):
                        return f
                    if identifier:
                        attrs = f.get_attributes()
                        if 'id' in attrs and attrs['id'] == identifier:
                            return f
            except:
                pass
        time.sleep(latency)
    return None

def getWindow(app, identifier, **kwargs):
    """Get a window with a given ID to show up. For instance:
    >>> w = getWindow(app, 'the_window')"""
    return _getWindow(app, identifier=identifier, **kwargs)

def getWindowPerName(app, name, **kwargs):
    """Get a window with a given name to show up. For instance:
    >>> w = getWindowPerName(app, 'The window')"""
    return _getWindow(app, name=name, **kwargs)

def _waitWindow(app, name=None, identifier=None, **kwargs):
    f = _getWindow(app, name, identifier, **kwargs)
    if f:
        if name and _nameMatch(name, f.name):
            passed("'%s' got active" % name)
            return

        if identifier:
            passed("'%s' got active" % identifier)
            return

    if identifier:
        failed("'%s' didn't get active" % identifier)
    else:
        failed("'%s' didn't get active" % name)
    for f in app:
        if f and f.getState().contains(pyatspi.STATE_ACTIVE):
            attrs = f.get_attributes()
            if 'id' in attrs:
                info("'%s' '%s' is active" % (f.name, attrs['id']))
            else:
                info("'%s' is active" % (f.name))

def waitWindow(app, identifier, **kwargs):
    """Wait for a window with a given ID to show up. For instance:
    >>> waitWindow(app, 'the_window')"""
    _waitWindow(app, identifier=identifier, **kwargs)

def waitWindowPerName(app, name, **kwargs):
    """Wait for a window with a given name to show up. For instance:
    >>> waitWindowPerName(app, 'The window')"""
    _waitWindow(app, name=name, **kwargs)

def waitDialog(app, identifier, **kwargs):
    """Wait for a dialog box with a given ID to show up. For instance:
    >>> waitDialog(app, 'the_dialog')"""
    _waitWindow(app, identifier=identifier, **kwargs)

def waitDialogPerName(app, name, **kwargs):
    """Wait for a dialog box with a given name to show up. For instance:
    >>> waitDialog(app, 'The dialog')"""
    _waitWindow(app, name=name, **kwargs)

def expectText(o, text):
    """Check that an object has an expected text. For instance:
    >>> acc = win.child(roleName='entry', identifier='my_entry')
    >>> expectText(acc, 'sometext')"""
    name = _getName(o)
    if o.text == text:
        passed("%s '%s' contains '%s'" % (o.getRoleName(), name, text))
    else:
        failed("%s '%s' does not contain '%s' but '%s'" % (o.getRoleName(), name, text, o.text))

def waitForText(o, text):
    """Wait for an object to contain an expected text. For instance:
    >>> acc = win.child(roleName='entry', identifier='my_entry')
    >>> waitForText(acc, 'sometext')"""
    name = _getName(o)
    for i in range(timeout):
        if o.text == text:
            passed("%s '%s' contains '%s'" % (o.getRoleName(), name, text))
            return
        # None yet, wait a bit
        time.sleep(latency)
    failed("%s '%s' does not contain '%s' but '%s'" % (o.getRoleName(), name, text, o.text))

def waitOutput(expect):
    """Wait for some output to be found on the braille device. For instance:
    >>> waitOutput("foo")"""

    global f

    print("waitOutput")
    # TODO: timeout
    while True:
        s = f.readline()
        if s[0:8] != 'Visual "':
            continue
        s = s[8:-2]
        info("got '%s'\n" % s)
        if s[0:len(expect)] == expect:
            passed("got output '%s' for '%s'" % (s, expect))
            return

    failed("didn't get output '%s'" % (expect))

def waitZoom(o):
    """Wait for zoom to switch to an object. For instance:
    >>> acc = win.child(roleName='menu', identifier='my_menu')
    >>> waitZoom(acc)"""

    if lastZoom != o:
        waitingZoom = o
        t = threading.Timer(switch_focus_timeout, pyatspi.Registry.stop)
        t.start()
        pyatspi.Registry.start(asynchronous=False, gil=True)
        t.cancel()
        waitingZoom = None
    name = _getName(o)
    if lastZoom == o:
        passed("zoom moved to '%s' '%s'" % (o.getRoleName(), name))
    else:
        failed("zoom didn't move to '%s' '%s'" % (o.getRoleName(), name))

def startBraille():
    """Start a virtual braille device. Call this before even starting the application
    For instance:
    >>> startBraille()"""

    global f

    # FIXME security
    path="/tmp/test-vr"
    ncells = 40
    try:
        os.unlink(path)
    except:
        pass
    p = subprocess.Popen(args=["brltty","-n","-b","vr","-d","server:"+path,"-x","no","-s","no","-p","none","-A","host=:10"])

    while True:
        try:
            s = os.stat(path)
        except:
            time.sleep(latency)
            continue
        break

    #print("got socket, connecting")
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0)
    s.connect(path)
    s.send(b"Cells %u\n" % ncells)
    f = s.makefile()
    #print("connected, running orca")

    os.environ["BRLAPI_HOST"] = ":10"
    p2 = subprocess.Popen(args=["orca", "--replace", "-d", "braille-monitor"], env=os.environ)
    #print("started orca")

    waitOutput("Screen reader on.")

    return p,p2,f

def stopBraille():
    """Stop the virtual braille device. For instance:
    >>> stopBraille()"""

    global p, p2

    p.kill()
    p2.kill()

def summary():
    """Print test summary and exit with error if at least one test failed.
    For instance:
    >>> summary()"""
    global npassed, nfailed
    print("")
    if nfailed:
        print("%d PASS" % npassed)
        print("%s%d FAIL%s" % (red, nfailed, normal))
    else:
        print("%s%d PASS%s" % (green, npassed, normal))
        print("%d FAIL" % nfailed)
    sys.exit(1 if nfailed else 0)

# Argl, we need to switch "page tab" to make other pages visible
def _setToReach(root, excludeRoles):
    s = set({})
    def __listToReach(o, i):
        print("%s %s '%s'" % (i*" ", o.getRoleName(), o.name))
        state = o.getState()
        if o.getRole() in toBeReachable and \
            o.getRole not in excludeRoles and \
            state.contains(pyatspi.STATE_ENABLED):
            print("%s to reach" % (i*" "))
            s.add(o)
        for o2 in o:
            __listToReach(o2, i+1)

    __listToReach(root, 0)
    return s


def testReachability(frame, start, excludeRoles):
    toreach = _setToReach(frame, excludeRoles)
    print("%d items to reach" % len(toreach))

    def try_change_focus(lastfocus, f):
        newfocus = None

        def focus_callback(event):
            nonlocal newfocus
            if event.source != lastfocus:
                newfocus = event.source
                pyatspi.Registry.stop()

        pyatspi.Registry.registerEventListener(focus_callback, "focus:")
        t = threading.Timer(switch_focus_timeout, pyatspi.Registry.stop)
        t.start()
        f()
        pyatspi.Registry.start(asynchronous=False, gil=True)
        t.cancel()
        pyatspi.Registry.deregisterEventListener(focus_callback, "focus:")
        return newfocus

    def try_change_focus_key(lastfocus, key):
        ret = try_change_focus(lastfocus, lambda: pressCombo(key))
        if ret == None:
            print("'%s' didn't switch" % key)
        return ret

    #keys = ['Tab', 'Right', 'Up', 'F6', '<shift>F6', 'Down', 'Left', '<shift>Tab']
    keys = ['Tab', 'Right', 'Up', 'Down', 'Left', '<shift>Tab']
    nkeys = len(keys)
    backkeys = keys.copy()
    backkeys.reverse()

    outgoing = {}
    def get_out(acc):
        if acc in outgoing:
            return outgoing[acc]
        l = nkeys*[None]
        outgoing[acc] = l
        return l

    ingoing = {}
    def get_in(acc):
        if acc in ingoing:
            return ingoing[acc]
        l = nkeys*[None]
        ingoing[acc] = l
        return l

    def grab_focus_trivial(acc):
        if acc.focused:
            return True
        ret = acc.grabFocus()
        if ret:
            waitFocus(acc)
            return True
        return False

    # TODO: not enough to get through a long left/right list
    # Really need to introduce a notion of focus group, to be reached.

    def grab_focus(acc):
        if grab_focus_trivial(acc):
            # It was trivial
            return True

        # It's not trivial, try to come forward from another object
        for i, other in enumerate(get_in(acc)):
            if other == None:
                continue
            if not grab_focus_trivial(other):
                continue
            if not waitFocus(other):
                continue
            # Managed to trivially focus that one, try to come back
            ret = try_change_focus_key(other, keys[i])
            if ret != acc:
                continue
            if waitFocus(acc):
                return True

        # Try to come back from another object
        for i, other in enumerate(get_out(acc)):
            if other == None:
                continue
            if not grab_focus_trivial(other):
                continue
            if not waitFocus(other):
                continue
            # Managed to trivially focus that one, try to come back
            ret = try_change_focus_key(other, backkeys[i])
            if ret != acc:
                continue
            if waitFocus(acc):
                return True

        return False

    l = [ start ]

    reached = set({})
    while l != []:
        newl = []
        print("new run with %d objects" % len(l))
        for cur in l:
            print("")
            print("looking at %s" % str(cur))
            ret = grab_focus(cur)
            if not ret:
                print("erfl, can't reach %s again" % str(cur))
                continue

            time.sleep(1)
            out = get_out(cur)

            for i, key in enumerate(keys):
                ret = grab_focus(cur)
                if not ret:
                    print("erfl, can't reach %s again" % str(cur))
                    break

                new = try_change_focus_key(cur, key)
                if new != None:
                    if waitFocus(new):
                        print("switched to %s" % str(new))
                        reached.add(new)
                        out[i] = new
                        get_in(new)[i] = cur
                        if new in toreach:
                            toreach.remove(new)
                            newl.append(new)
        l = newl
    for o in toreach:
        print("didn't reach %s" % str(o))
    return

def slowDown():
	config.typingDelay = 0.1
	config.defaultDelay = 0.5

def speedUp():
	config.typingDelay = 0
	config.defaultDelay = 0
