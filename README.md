Application behavior Regression testing
=======================================

This series of tools allows to check the user-visible behavior of applications.

The principle is to start the application, then run a scenario testing script,
which synthesizes keypresses, and checks that the expected behavior occurs.


Dependencies
------------

You need

python3-dogtail (from deb http://people.debian.org/~sthibault/tmp/sid-tmp ./ )
fluxbox
xvfb
xauth
dbus-x11
tigervnc-standalone-server
tigervnc-viewer

For the thunderbird tests, you need

hunspell-en-us

It's better to have

- libatspi2.0-0 >= 2.33.1, or patched by hypra (at least +hypra2)
- libatk-adaptor:amd64 libatk-bridge2.0-0:amd64 >= 2.33.1, or patched by hypra (at least +hypra2)
- libatk1.0-0:amd64 >= 2.33.1, or patched by hypra (at least +hypra2)
- python3-pyatspi >= 2.33.1, or patched by hypra (at least +hypra2)
- libgtk-3-0 patched by hypra (at least +hypra5)

in order to get the stable accessible IDs of widgets.


Running
-------

One can run tests with:

    cd thunderbird
    ../run ./thunderbird-simpletest

This runs a virtual X server, the application on the X server, and runs the
testsuite on it. In the terminal where one has started the run command, we can
see the progression of the tests.  In order to see what is happening on the
virtual X server, one can use this instead:

    ../run-viz ./thunderbird-simpletest

Which will open a window to show what is happening on the virtual server.  Since
the test goes through interfaces as quickly as possible, it is hard to follow
what is happening, but in case it gets stuck at some point, it allows to see
what happens exactly. Using `run-act` instead of `run-viz` allows to act on the
test, to more easily determine the state of the application for instance.
For more fine-grained debugging, `run-debug` allows to run the test suite within
pdb, where it can be executed step-by-step or line-by-line much like in gdb.

Scenarios are grouped by 'modules', 'lessons' or 'chapters', that can be passed
as an argument (or multiple args) to the script.  A rough list of these modules
may be obtained by passing the test script’s path to `run-list`, as follows:

    ../run-list ./thunderbird-test

By default the script uses the thunderbird version from /usr/bin. This can be
changed by modifying the thunderbird-start init file.


Benchmarks
----------

Two benchmarks are available to get an idea of the testing speed:

    cd libreoffice
    ../run ./lowriter-bench
    ../run ./lowriter-bench-orca

They both cycle with F6 in libreoffice writer, and print the delay to perform
one cycle.  The orca version waits for the output to show up in Orca


Existing tests
--------------

Tests written so far include:

- thunderbird/thunderbird-simpletest whose purpose is to be a simple example
  which just presses F6 several times and checks that the proper panels are
  getting focused.

- thunderbird/thunderbird-main which presses tab and shift-tab several times and
  checks that the proper widgets are getting focused.

- thunderbird/thunderbird-test which runs through the use cases of the SAUI
  ("Savoir Accéder et Utiliser Internet") chaper 5.

- lowriter/lowriter-test which for now presses F6 several times and checks that
  the proper panels are getting focused.


Writing dogtail tests
---------------------

One can write tests by hand by using the functions of the regression module, see

    pydoc3 regression

In general, the principle is to call regression.press() or
regression.pressCombo(), and check the resulting behavior with
regression.waitFocus(), regression.waitSelected(), etc. For instance:

    regression.press(acc, 'Tab')
    acc = regression.child(win, 'entry', 'dateLabel')
    regression.waitFocus(acc)
    regression.expectText(acc, '01/01/2019')

Checks that pressing Tab gets focus to the dateLabel entry, and that it contains
the text "01/01/2019".

One can for instance read thunderbird-simpletest which is a rather simple test
for F6 navigation.


One can also generate test scripts by using the "log" script: first start
the application by hand in the C locale (with export LC_ALL=C), or with
./lowriter-start or ./thunderbird-start (these properly starts them in the C
locale and in safe mode to avoid any personal configuration bits), and then run
../log. log will print keyboard events and focus events while you perform them
in the application. It will however only start printing after you have typed
left control three times, and it will stop printing on typing right control
three times. That way you can easily isolate the test in the log output before
copy/pasting in your test script. In the firefox and thunderbird cases, after
you type left control three times, switch to the tested application via
alt-tab ; do not issue alt-tab before triple-left control or the script will
not be written properly.

You will probably want to clean the log output to remove useless parts or at
least introduce empty lines to gather related tests. In general, remove the line
related to the start of the application you come from with alt-tab. For further
information about cleaning the recorded tests, see the section below.
 


The test itself can then be written like this:

  #!/usr/bin/python3
  from dogtail.tree import root
  from dogtail.utils import run
  import os
  import sys
  sys.path.insert(1, os.path.pardir)
  import regression

to import the required python modules, then

  os.environ["LC_ALL"] = "C"  
  run('/usr/bin/pluma', appName='pluma')

to start the application in the C locale (here pluma for instance) and wait for
it to become accessible. The correct appName can be seen in the log output when
starting the application.

The principle is then that the log script keeps updating three variables and
uses them to observe the behavior:

- app contains the at-spi object for the current application. It is used to find
out windows.
- win contains the at-spi object for the current window. It is used to find out
objects.
- acc contains the last object that was observed.


There are different ways of reaching an at-spi object:

- Getting it by accessible ID:

  acc = regression.child(win, 'entry', 'dateLabel')

This is the preferred way, because it is unambiguous, and does not depend on the
wording of the user interface, only on the programming IDs which are usually
stable over development.

- Getting it by name:

  acc = regression.childPerName(win, 'entry', 'Date')

This is less preferred because it is ambiguous, depends on the locale and
wording.

- Getting it by parent:

  parent = regression.child(win, 'section', 'dateInput')
  acc = regression.childPerRole(parent, 'entry')

It can happen that while the targetted object does not have an accessible ID,
some of its parents do, and only the targetted object has a given role. In that
case we can safely get the parent, and then lookup the only child which has the
given role.

- Getting it by parent and name:

  parent = regression.child(win, 'section', 'dateInput')
  acc = regression.childPerName(parent, 'Date')

This is not preferred because it depends on the locale and wording, but at least
it is less ambiguous than getting only by name, because the parent probably does
not have several children with the same name.


The log script always uses the best way to reach at-spi objects.


It is useful to put lines such as

  regression.title("starting tests foo")

which will print the given text during the test run, to give a view of the test
progression.


Recording for the whole desktop
-------------------------------

When recording a scenario for the desktop itself, one should 

- run the desktop as a user that has no personalization.
- ideally, run log outside of the graphical session, to avoid the presence of
the graphical terminal in the desktop. One can log as the user on the Linux
virtual terminal and run `export DISPLAY=:0` before starting ./log.
- the scenarii should always start from the same situation before typing the
triple-control, e.g. focus on the desktop without any application running.

Note: ATM the alt-f1 and alt-f2 shortcuts are captured by the window manager, so
it will not be recorded in the log, so one has to remember to add them by hand
in the log :/


Cleaning recorded tests
-----------------------

First of all, if you insert the test scenario in an existing file (eg. thunderbird-test):
- be careful with the indentation: if you are sure of what you do, you can insert
your scenario in a conditions after you are surethe point of starting is the first
status of the applicatin interface.If you do so, apply to the file you are
about adding containing your scenario:
sed -e 's/^/    /'

- the first line of your inserted scenario should be:
    regression.title('Create a new mail account')

    win = regression.child(app, 'frame', 'messengerWindow')  # Draft - Mozilla Thunderbird

Moreover the 'log' script may emit spurious parts, which need to be cleaned up.  We could
notice at least:

- 'log' will always emit lines for starting the application.  When combining
several recordings, one should drop such lines, so that the sequence is
coherent. More precisely, one can paste a new sequence only if its starts with
the same focused object as what is obtained at the end of the existing sequence.

- keep only one instance of the "summary()" call, which terminates the complete
script.

- when the test goes through menu items, dialog buttons, or tree items, there
is no need to keep all arrow key press events. 'log' prints a corresponding
selectButton / selectMenuItem / selectTreeItem / selectPageTab call which can be
used instead to reach the target element.

- in the case of page tabs, unfortunately software sometimes remember which one
was selected last. regression.py can handle selecting a page tab starting from
anywhere, but when entering the dialog box the initially focused object will be
on the first widget of the tab that was last opened by the user. One thus have
to remove that focus check, i.e. drop anything between the dialog opening check
and the shift-tab key press to get on the page tab list. Similarly, sometimes
the first widget of the initial page tab gets the focus, sometimes it's the
initial page tab itself which gets the focus. One thus may have to insert a

    regression.pressCombo(win, '<shift>Tab')

to make sure to come back to the page tab list.

- if during recording the user typed quite fast, a text entry content update
corresponding to a given keypress might have been recorded late, after the next
keypress. One has to put it back before the next keypress, to really get an
alternation of key press / expected result, otherwise the expected entry content
update will not match.

- mozilla software seems to be exposing two at-spi elements for windows, one
which has an id, and another one which does not. The latter is generally not useful, so one may have to remove tests against it, e.g.:

    regression.waitWindow(app, 'accountManager') # Account Settings
    win = regression.child(app, 'dialog', 'accountManager')  # Account Settings

    regression.waitWindowPerName(app, 'Account Settings')
    win = regression.childPerName(app, 'frame', 'Account Settings')

The second paragraph is useless (and possibly problematic since it depends on
the title of the window).

- The software behavior may depend on external configuration (e.g. default name
for the user).

- One may need to introduce delays when the software needs time to update the
window content.

- thunderbird seems to spuriously advertise focus on "document web" when writing
a message.

- Since names may vary, you may want to extend matches into regexps (inserting e.g. `.*` in the name pattern)

The definite test, of course, is to just run the generated test, and check
it does not hang or fail, and if it does, see what is going wrong at that
point. The thunderbird-test script is split in sections separated by an if
statement, one can thus run "../run-viz ./thunderbird-test browse" to test
folder browsing for instance.

- For libreoffice, pieces of tests should be finished with pressing control-F4
to shut down the modified document. This key press and anything beyond that
should be replaced by a reset() call which properly closes the document,
avoiding saving it, and creating a new document.

- For libreoffice, the document role has changed at some version, the
`document_text` variable contains the proper role according to the version. Any
new test recorded with libreoffice 4 should thus get a pass of

s/childPerName(win, 'document frame', 'Document view')/childPerRole(win, document_text)

to replace the LO4-specific role with the generic role

- When switching between toolbars in LibreOffice with F6, between the first and
the second bar, some panel gets activated, which log doesn't catch, and thus the
generated script thinks we are back to the first bar. This has to be fixed
into not checking for focus and just press F6 again without waiting.

- When a test can't be cleaned, one can still leave it in the file, just not enabled by default, by using:

    if 'foobar' in testlist:

instead of

    if not testlist or 'foobar' in testlist:



General advice
--------------

Since tests are started from the initial state of the application, test cases
would need to be recorded from there, to generate all keypresses etc. to reach
what one wants to test.

It is of course possible to factorize a bit.  For instance, one can decide to
have all bookmark subtests start from the main bookmark window.  The overall
test should thus be made to start with pressing the shortcut to open the
bookmark window, and proceed with several subtests, each of which always returns
to that bookmark window.  This way, one can record several subtests without the
hassle of restarting from the main window each time.


The log script will both print keypresses (e.g. "press(acc, 'tab')") and a way
to achieve it more safely (e.g. "selectMenuItem(acc, 'openItem')"). One does not
need to keep both in the test, usually you would prefer to keep the latter.


Note on thunderbird tests
-------------------------


Using the profile from this repository
......................................

Testing thunderbird usually requires having an account configured, a few mails
to manipulate, etc.  This repository includes a profile which has this.  A fresh
copy is made on each thunderbird-start invocation, so that the content of
folders, mails etc. is reproducible.


Using the thunderbird testsuite profile
.......................................

In order to create tests that can be included in the thunderbird suite (see
below), one needs to use the standard profile used in that testsuite. That can
be easily achieved by:

- getting a configured checkout of thunderbird (expect like 10GB disk use):

   hg clone https://hg.mozilla.org/mozilla-central/ thunderbird
   cd thunderbird hg clone http://hg.mozilla.org/comm-central/ comm
   ./mach configure

(you need the mercurial package to get the hg command)

- creating a dumb test:

  echo "import time" > test_hang.py
  echo "time.sleep(100000)" >> test_hang.py

- running it:

  ./mach marionette-test --binary /usr/lib/thunderbird/thunderbird test_hang.py

That will start thunderbird and let it run for days so you can play with it and
get test log from the log script.


Producing marionette tests
..........................

Tests can be produced for the thunderbird marionette test suite by using the
dogtail2marionette conversion script, which just executes a sed script on its
input.  It can then be put in a test_foo() function in a new file similar to
comm/mail/test/marionette/test_empty.py, and put along that file. The name
of the new file can then be put in manifest.ini in the same directory, so it
will get executed during the testsuite. The name of the class does not matter as
long as it starts with "Test". The name of the function does not matter either
as long as it starts with "test_".

One can then run it with e.g.

  ./mach marionette-test --binary /usr/lib/thunderbird/thunderbird comm/mail/test/marionette/test_mytest.py


Using thunderbird nightly
.........................

In order to produce tests against the latest version of thunderbird, one
needs to use a nightly build rather than the build from your system (since
the exact behavior and widget names may differ). You can download it from
https://archive.mozilla.org/pub/thunderbird/nightly/ , unpack it in /tmp for
instance and then use

  ./mach marionette-test --binary /tmp/thunderbird/thunderbird comm/mail/test/marionette/test_mytest.py

This can notably be used with various nightly versions to check when something
got broken.


Building thunderbird
....................

If making your test succeeds needs to fix thunderbird (i.e. nightly fails),
you need to build thunderbird yourself (expect like 30GB disk use). From your
checkout of thunderbird, run

   hg clone https://hg.mozilla.org/mozilla-central/ thunderbird
   cd thunderbird
   hg clone http://hg.mozilla.org/comm-central/ comm
   ./mach build

(you need the mercurial package to get the hg command)

and then you can run your test with e.g.

  ./mach marionette-test comm/mail/test/marionette/test_mytest.py

or the whole testsuite with using

   xvfb-run ./mach marionette

It is recommended to do that before submitting, to check in its log that your
test is indeed run along the testsuite, and suceeds.


Some important limitations in marionette
........................................

- Using platform-specific dialog boxes will not work. For instance, attach files
  will not work, because since they are implemented directly in gtk, they do not
  support marionette.

- Due to https://bugzilla.mozilla.org/show_bug.cgi?id=1545460 ,
  opening modal dialog boxes has to be done through arrows in menus, not
  keyboard shortcuts. Non-modal windows (composing a message, addressbook, etc.)
  work fine.
